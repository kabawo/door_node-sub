import time
import subprocess
import paho.mqtt.client as paho
import sys, pygame
from pygame.locals import *

pygame.init()
gameDisplay = pygame.display.set_mode((260,120))
pygame.display.set_caption('Door control')

broker="iot.eclipse.org"

def text_objects(text, font):
    textSurface = font.render(text, True, (122,122,122))
    return textSurface, textSurface.get_rect()

def on_connect(client,userdata,flags,rc):
    if rc == 0:
        print("Connected")
        global Connected
        Connected = True
    else:
        print("Connection failed")

def on_message(client,userdata,message):
    message_received = str(message.payload.decode("utf-8"))
    print(message_received)
    global DoorOpen
    if "open" in message_received:
        DoorOpen = True
    else:
        DoorOpen = False
    subprocess.call(['/usr/bin/canberra-gtk-play','--id','bell'])

Connected = False
DoorOpen = False

client = paho.Client("mojlaptop123")
client.on_connect = on_connect
client.on_message = on_message
client.connect(broker)

def show_control(door_open):
    doortext = pygame.font.Font('freesansbold.ttf',22)
    TextSurf, TextRect = text_objects("Filip's room", doortext)
    TextRect.bottomleft = (30,74)
    gameDisplay.blit(TextSurf,TextRect)
    pygame.draw.rect(gameDisplay, (255,255,255), (178,48,44,24))
    pygame.draw.rect(gameDisplay, (100,100,100), (180,50,40,20))
    if door_open:
        pygame.draw.rect(gameDisplay, (0,255,0), (180,50,20,20))
    else:
        pygame.draw.rect(gameDisplay, (255,0,0), (200,50,20,20))
    pygame.display.update()

client.loop_start()

while Connected != True:
    time.sleep(0.1)

client.subscribe("16023740/door/front")

try:
    while True:
        show_control(DoorOpen)
        time.sleep(0.2)
except KeyboardInterrupt:
    print ("exiting")
    client.disconnect()
    client.loop_stop()
